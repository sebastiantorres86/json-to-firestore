const admin = require('./node_modules/firebase-admin')
const serviceAccount = require('./serviceAccountKey.json')

const data = require('./data.json')
const collectionKey = 'Patient_Data' // nombre de la colección

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount)
})

const firestore = admin.firestore()
const settings = { timestampsInSnapshots: true }
firestore.settings(settings)

if (data && typeof data === 'object') {
  Object.keys(data).forEach(id => {
    firestore
      .collection(collectionKey)
      .doc()
      .set(data[id])
      .then(res => {
        console.log('Document ' + id + ' successfully written!')
      })
      .catch(error => {
        console.error('Error writing document: ', error)
      })
  })
}
