# Cómo subir datos a Firebase Firestore Cloud Database

Firestore es una base de datos simple y sofisticada que ayuda a almacenar nuestros datos en formato de documento en la nube. Proporciona una API muy fácil de usar para realizar todas las operaciones CRUD (Crear, Leer, Actualizar, Eliminar). Pero durante la fase inicial del proyecto, es posible que deseemos cargar algunos datos maestros o de configuración de forma masiva. Actualmente no hay una opción de carga directa de datos proporcionada en la base de datos de Firestore, por lo que necesitamos hacer un poco de codificación para lograrlo. Empecemos.

- **Paso 1:** Prepare sus datos en formato JSON. Puede usar una
  herramienta de creación de datos simulados como [mockaroo](https://www.mockaroo.com/):

![](/images/1.png)

- **Paso 2:** Descarga la clave de servicio de tu proyecto de Firebase. Abra su proyecto de Firebase > haga clic en el ícono de configuración > seleccione "Users and permissions"

![](/images/4.png)

Dentro de Configuración, haga clic en "**Service accounts**". Desde la barra de pestañas del lado izquierdo, seleccione "Firebase Admin SDK", y luego presione el botón "**Generate Private Key**" en la parte inferior de la página, se descargará el archivo JSON de la clave de la cuenta de servicio.

![](/images/5.png)

Nuestra clave de cuenta de datos y servicio está lista, así que preparemos el programa de carga.

![](/images/7.png)

- **Paso 3:** Configura un proyecto node.js para cargar tu archivo de datos JSON en la base de datos de Firestore. Cree una carpeta de proyecto en su ubicación preferida. Abra el terminal o el símbolo del sistema y navegue dentro de esa carpeta y ejecute el comando **npm init**

![](/images/8.png)

Después de ejecutar el comando anterior, puede ver que se crea un archivo "**package.json**" dentro de la carpeta de su proyecto. Coloque los archivos "**data.json**" y "**serviceAccountKey.json**" dentro de la carpeta de su proyecto. Abra la carpeta de su proyecto en cualquiera de sus editores IDE favoritos (mi favorito es [Visual Studio Code](https://code.visualstudio.com/)) y cree el archivo "**index.js**".

![](/images/9.png)

Necesitamos escribir nuestro programa de carga dentro de este archivo "index.js". Antes de continuar, necesitamos instalar la biblioteca "firebase-admin". Instálelo usando el siguiente comando desde la terminal, debe ejecutar el comando desde la carpeta raíz de su proyecto.

```bash
npm install firebase-admin
```

![](/images/10.png)

Después de la instalación, puede ver una carpeta "**node_modules**" creada dentro de la carpeta de su proyecto, y una dependencia (**firebase-admin**) agregada dentro de su archivo package.json.
Ahora copie el código siguiente y péguelo dentro de su archivo **index.js**. Recuerde reemplazar los valores de **collectionKey** (nombre de la colección) y **databaseURL** (que anotó durante la descarga de la clave de la cuenta de servicio) por los suyos.

```javascript
const admin = require("./node_modules/firebase-admin");
const serviceAccount = require("./serviceAccountKey.json");
const data = require("./data.json");
const collectionKey = "entities"; // nombre de la colección

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
});

const firestore = admin.firestore();
const settings = { timestampsInSnapshots: true };
firestore.settings(settings);
if (data && typeof data === "object") {
  Object.keys(data).forEach((docKey) => {
    firestore
      .collection(collectionKey)
      .doc(docKey)
      .set(data[docKey])
      .then((res) => {
        console.log("Document " + docKey + " successfully written!");
      })
      .catch((error) => {
        console.error("Error writing document: ", error);
      });
  });
}
```

![](/images/code.png)

## Explicación del código

- Línea 1: Importando la biblioteca firebase-admin
- Línea 2: Importación de información clave de la cuenta de servicio
- Línea 4: Importación de archivo de datos JSON
- Línea 5: Configuración del nombre de la colección. por favor modifique esta línea y configure su propio nombre de colección
- Línea 6-8: inicialización de admin sdk
- Línea 10-14: Configuración de la propiedad Firestore
- Línea 16: Verificando si existe algún dato en el archivo de datos y si es de tipo de objeto
- Línea 14: bucle en el objeto de datos
- Línea 15: Llamar a la API de Firestore para almacenar cada documento
- Línea 16: Configuración del nombre de la colección
- Línea 17: Configuración del nombre/clave del documento. No proporcione la clave del documento si desea que la clave del documento se genere automáticamente.

![](/images/13.png)

- Línea 18: Configuración de datos del documento
- Línea 20: mensaje de éxito, después de que el documento se haya almacenado correctamente en la base de datos de Firestore
- Línea 23: mensaje de error si falla

- **Paso 4:** Ejecute su programa. En la terminal, suba un nivel desde la carpeta de su proyecto y ejecute el comando debajo para ejecutar su programa de carga:

```node
node <Your_Project_Folder_Name>
```

![](/images/14.png)

![](/images/15.png)

Eso es todo, hemos terminado.

_happy coding…_
